import React from 'react';
import { VERSION } from '@twilio/flex-ui';
import { FlexPlugin } from 'flex-plugin';

import CustomTaskListContainer from './components/CustomTaskList/CustomTaskList.Container';
import reducers, { namespace } from './states';
import SamplePlugin from './SamplePlugin';
import MyComponent from './MyComponent';
import MyButton from './MyButton';

const PLUGIN_NAME = 'SamplePlugin';

export default class CustomePlugin extends FlexPlugin {
  constructor() {
    super(PLUGIN_NAME);
    console.log(this, '234234241234213')

  }

  /**
   * This code is run when your plugin is being started
   * Use this to modify any UI components or attach to the actions framework
   *
   * @param flex { typeof import('@twilio/flex-ui') }
   * @param manager { import('@twilio/flex-ui').Manager }
   */

  init(flex, manager) {
      console.log(flex, 'flexxxxxxxxx', manager, this, 'thisss')
      console.log(Window, 'windowwwwww')

    this.registerReducers(manager);

    flex.CRMContainer.defaultProps.uriCallback = (task) => {
        console.log(task && task.source, 'taskkkkkktaskkkkkktaskkkkkktaskkkkkktaskkkkkk')
      return task 
        ? `https://bing.com/?q=${task.attributes.name}`
        : 'https://bing.com/';
    }

    flex.TaskList.defaultProps.uriCallback = (task) => {
        console.log(task.source, 'taskkkkkk')
        return task 
        ? `https://bing.com/?q=${task.attributes.name}`
        : 'kamleshhhhh';
    }

    flex.MainHeader.Content.add(<MyComponent key="mute"/>, {
        sortOrder: 1, 
        align: "end"
      });

      
    flex.MainHeader.Content.add(<MyButton key="demo-component"/>, {
        sortOrder: 40,
        align: "start" //no "center" option is available
    });

    flex.MainHeader.Content.remove("mute");

    flex.MainHeader.Content.add(<MyComponent key="mute"/>, {
        if : props =>{
            console.log( props, ' props.task.source.taskChannelUniqueName === "cu')
       return props.task && props.task.source && props.task.source.taskChannelUniqueName && props.task.source.taskChannelUniqueName === "custom1" ;
        } 
      });

      
  }

  /**
   * Registers the plugin reducers
   *
   * @param manager { Flex.Manager }
   */
  registerReducers(manager) {
    if (!manager.store.addReducer) {
      // eslint: disable-next-line
      console.error(`You need FlexUI > 1.9.0 to use built-in redux; you are currently on ${VERSION}`);
      return;
    }

    manager.store.addReducer(namespace, reducers);
  }
}